# 13 - APIs e Banco de Dados

## MongoDB
Vejamos mais alguns métodos úteis do MongoDB.

### Sort
O método `sort()` ordena um resultado em order cresecente ou descresecente, usando um objeto como parâmetro que define a ordenação.
Use `1` para ordernar em order cresecente e `-1` para descresecente.

```js
{ key: 1 } // ascending
{ key: -1 } // descending
```

Exemplo:

```js
const { MongoClient } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/', (error, db) => {
  if (error) throw error;

  const dbo = db.db('mydb');

  dbo.collection('customers').find().sort({ name: 1 }).toArray((error, result) => {
    if (error) throw error;

    console.log(result);
    db.close();
  });
});
```

### Limit
Para limitar o resultado de uma consulta, usamos o método `limit()`, que aceita um parâmetro númerico, que define a quantidade de documentos à retornar.
Exemplo:

```js
const { MongoClient } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/', (error, db) => {
  if (error) throw error;

  const dbo = db.db('mydb');

  dbo.collection('customers').find().limit(5).toArray((error, result) => {
    if (error) throw error;

    console.log(result);
    db.close();
  });
});
```

### Drop
Você pode excluir uma tabela ou coleção, como é chamada no MongoDB, usando o método `drop()`.
O método `drop()` recebe uma função `callback` contendo o objeto de erro e um parâmetro de resultado que retorna `true` se a coleção foi deletada ou `falso`, caso contrário.

```js
const { MongoClient } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/', (error, db) => {
  if (err) throw err;
  const dbo = db.db('mydb');

  dbo.collection('myCollection').drop((error, deleted) => {
    if (error) throw err;
    if (deleted) console.log('Collection deleted');
    db.close();
  });
});
```

## Desafio

Neste desfafio vocês devem criar endpoints que conversem com o banco de dados, ou seja, criar endpoints com o Express que se comunique com o MongoDB.

### Banco de Dados

O banco deverá ter duas entidades: **Aluno**, **Curso**

**Aluno**
- id: string
- nome: string
- sobrenome: string
- idade: number
- idCurso: string (Regra: Um aluno só pode ter um curso)

**Curso**
- id: string
- nome: string
- diasDaSemana: enum (Regra: Sábado e Domingo devem dar erro e apresentar uma mensagem de erro)
  - Segunda
  - Terça
  - Quarta
  - Quinta
  - Sexta

### Endpoints

- Busca de Alunos (mais de um tipo de busca não pode acontecer ao mesmo tempo)
  - Por ID
  - Por nome e sobrenome _*_
  - Por ID do curso _*_

- Busca de Curso (mais de um tipo de busca não pode acontecer ao mesmo tempo)
  - Por ID
  - Por dia da semana _*_

- Cadastro de Alunos
- Cadastro de Cursos

- Alterar aluno de curso (enviar os IDs dos registros)
- Alterar curso para outro dia da semana (enviar os IDs dos registros) _*_

_* Para se destacar_

Para a entrega, **exportem** o banco de dados **já populado** para podermos realizar testes mais facilmente e coloquem no repositório.

### Docs:
- [Criando um CRUD completo com NodeJS, Express e MongoDB](https://medium.com/baixada-nerd/criando-um-crud-completo-com-nodejs-express-e-mongodb-parte-1-3-6c8389d7147d)
- [Import and Export Data](https://docs.mongodb.com/compass/master/import-export)
